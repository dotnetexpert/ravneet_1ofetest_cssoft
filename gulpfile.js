var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
 var clean = require('gulp-clean');
var browserSync = require('browser-sync').create();
var ts = require('gulp-typescript');

gulp.task('sass', function() {
  return gulp.src('app/index.scss') // Gets all files ending with .scss in app/scss and children dirs
    .pipe(sass())
    .pipe(gulp.dest('app'))
})
gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: 'app'
    },
  })
})

gulp.task('clean-scripts', function () {
  return gulp.src('app/dist/*.js', {read: false})
    .pipe(clean());
});
gulp.task('javascript',['clean-scripts'], function() {
  return gulp.src(['app/**/*.js','!./node_modules/**'])   
      .pipe(concat('all.js'))   
     .pipe(gulp.dest('app/dist'));
});



gulp.task('ts', function(done) {
  var tsResult = gulp.src("app/**/*.ts")
    //.pipe(ts(tsProject), undefined, ts.reporter.fullReporter());
  return tsResult.js.pipe(gulp.dest('app/tsfile'));
});

// gulp.task('watch', ['browserSync', 'sass'], function (){
//   gulp.watch('app/scss/**/*.scss', ['sass']); 
//   // Reloads the browser whenever HTML or JS files change
//   gulp.watch('app/*.html', browserSync.reload); 
//   gulp.watch('app/**/*.js', browserSync.reload); 
// });
// 

