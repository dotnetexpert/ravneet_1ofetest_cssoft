

app
.controller('CountCtrl', ["zmProfileGroupsService","$scope", function CountCtrl(zmProfileGroupsService,$scope) {
    //zmProfileGroupsService.readListOfGroups().then(function (response) {
    //    debugger;
    //    $scope.groups =response.data;
    //})
}])
    .controller('GroupCrtl', ["zmProfileGroupsService", "$scope", "$rootScope", "$filter", function CountCtrl(zmProfileGroupsService, $scope, $rootScope, $filter) {
        debugger;
        //$scope.$watchCollection(
        //         "groups",
        //         function (newValue, oldValue) {
        //             $scope.groups = newValue;
        //             //addLogItem($scope.watchCollectionLog);
        //         }
        //     );
        zmProfileGroupsService.readListOfGroups().then(function (response) {
           
            $scope.groups = response.data;
            //$scope.dragOptions = {
            //    start: function (e) {
            //        console.log("STARTING");
            //    },
            //    drag: function (e) {
            //        console.log("DRAGGING");
            //    },
            //    stop: function (e) {
            //        console.log("STOPPING");
            //    },
            //    container: 'container'
            //}
            
            $rootScope.dropped = function (dragElID, dropElID, dragElName, dropElName) {
                debugger;
                var droppedGroup = $filter('filter')($scope.groups, { groupid: dropElID })[0];
             
                var droppedUser;
                var isCheck = true;
                var mainOrNot=false;
                angular.forEach(droppedGroup.lists, function (key, value) {
                   
                   
                    if(isCheck==true){
                    if (key.main == false) {
                        droppedUser = $filter('filter')(key.people, { userid: dragElID })[0];
                        mainOrNot = true;
                    }
                    else {
                        droppedUser = $filter('filter')(key.people, { name: dragElName })[0];

                    }
                    if (droppedUser != undefined || droppedUser != null)
                        isCheck = false;
                    }

               
                   
                        
                })
               
                if (droppedUser != undefined || droppedUser != null)
                    alert("This user is already present in this group.")
                else
                {
                    var userDropped = $filter('filter')($rootScope.AllUsers, { userid: dragElID })[0];
                    var user;
                    var list;
                    if(mainOrNot==true)
                    {
                        list = $filter('filter')(droppedGroup.lists, { main: true })[0];
                        user = { "name": userDropped.username, "type": "user", "avatar": "assets/images/avatars/andrew.jpg" }
                       
                        
                    }
                    else
                    {
                        list = $filter('filter')(droppedGroup.lists, { main: false })[0];
                        user = { "name": userDropped, "userid": userDropped.userid, "type": "user", "avatar": "assets/images/avatars/andrew.jpg" }
                       
                    }
                    list.people.push(user);
                    zmProfileGroupsService.addUserToGroup($scope.groups).then(function (response) {
                        debugger;
                    },
                    function (a, b, c) {
                        debugger;
                    })
                }
             

                // this is your application logic, do whatever makes sense
                //var drag = angular.element(dragEl);
                //var drop = angular.element(dropEl);
                //var aa = drag.attr('title');
                //console.log("The element " + drag.attr('title') + " has been dropped on " + drop.attr("title") + "!");
            };
        })
    }])
    .controller('GroupUserCrtl', ["zmProfileGroupsService", "$scope", "$rootScope", function CountCtrl(zmProfileGroupsService, $scope, $rootScope) {
        debugger;
        zmProfileGroupsService.readGroupUsers().then(function (response) {
            debugger;
            $scope.groupsUsers = response.data;
            $rootScope.AllUsers = response.data;
        })
    }])
    //.controller('GroupDetailCrtl', ["zmProfileGroupsService", "$scope", function CountCtrl(zmProfileGroupsService, $scope) {
    //    debugger;
    //    zmProfileGroupsService.readGroupDetail().then(function (response) {
    //        debugger;
    //        $scope.groupsUsers = response.data;
    //    })
    //}])
	.component('zmProfileGroups', {
	    bindings: {
	        userid: '='
	    },
	   controller:'GroupCrtl',
	    templateUrl: 'GroupComponent.html'
	})
.component('zmProfileGroupsDetail', {
    bindings: {
        users: '='
    },
    controller:'GroupUserCrtl',
    templateUrl: 'GroupUsersComponent.html'
})
.component('zmProfileGroupsGroupListItem', {
    bindings: {
        group: '='
    },
    controller: 'GroupCrtl',
    templateUrl: 'GroupDetailComponent.html'
})
;

