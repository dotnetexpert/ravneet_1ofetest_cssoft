﻿app
.

factory('zmProfileGroupsService', ['$http', '$q', function ($http, $q) {

   

    var authServiceFactory = {};

    var _readListOfGroups = function (userId) {
       
        debugger;
        var deferred = $q.defer();
       
        $http({

            method: 'GET',
            url: 'groups.json',
            
           
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var _readGroupDetail = function () {

        var deferred = $q.defer();
       
        $http({

            method: 'GET',
            url: 'groups.json',

           
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (error) {
            deferred.reject(error);
        });
        return deferred.promise;

    }

    var _readGroupUsers = function () {

        var deferred = $q.defer();
       
        $http({

            method: 'GET',
            url: 'contacts.json',

           
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var _addUserToGroup = function (group) {
        debugger;
        var deferred = $q.defer();
        var data = '"data":' + group + '';
        $http({

            method: 'POST',
            url: 'groups.json',
            data: JSON.stringify(data)

        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
    authServiceFactory.readListOfGroups = _readListOfGroups;
    authServiceFactory.readGroupDetail = _readGroupDetail;
    authServiceFactory.readGroupUsers = _readGroupUsers;
    authServiceFactory.addUserToGroup = _addUserToGroup;
    return authServiceFactory;

}]);