'use strict';

// Declare app level module which depends on views, and components

var app = angular.module('app', [
    'ui.router',
    'ngMaterial',
    'lvl.directives.dragdrop'
    
]);


app.config(['$stateProvider', '$urlRouterProvider',
      function ($stateProvider, $urlRouterProvider) {
          $urlRouterProvider.otherwise("/profile");
          $stateProvider.state('profile', {
              url: "/profile",
              templateUrl: "profile/profile.html"
          })
      }
])
   
   

;